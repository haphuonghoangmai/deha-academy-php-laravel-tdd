<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_task() {
        $user = User::factory()->create();
        $this->actingAs($user);
        $task = Task::latest('id')->first();
        $response = $this->get(route('tasks.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->title);
    }

    /** @test */
    public function unauthenticated_user_can_get_list_task() {
        $task = Task::latest('id')->first();
        $response = $this->get(route('tasks.index'));

        $response->assertRedirect('login');
    }
}
