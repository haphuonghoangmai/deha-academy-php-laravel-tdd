<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_create_new_task()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $task = Task::factory()->make()->toArray();
        $response = $this->post(route('tasks.store'), $task);

        $this->assertDatabaseHas('tasks', $task);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_new_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post(route('tasks.store'), $task);

        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_user_can_not_create_new_task_if_title_null()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $task = Task::factory()->make(['title' => null])->toArray();
        $response = $this->post(route('tasks.store'), $task);

        $response->assertSessionHasErrors('title');
    }

    /** @test */
    public function authenticated_user_can_not_create_new_task_if_content_null()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $task = Task::factory()->make(['content' => null])->toArray();
        $response = $this->post(route('tasks.store'), $task);

        $response->assertSessionHasErrors('content');
    }

    /** @test */
    public function authenticated_user_can_not_create_new_task_if_title_and_content_null()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $task = Task::factory()->make(['title' => null, 'content' => null])->toArray();
        $response = $this->post(route('tasks.store'), $task);

        $response->assertSessionHasErrors(['title', 'content']);
    }
}
